/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.happycube.Cube;
import com.happycube.Piece;
import com.happycube.Util;
import java.io.IOException;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * This class tests methods in com.happycube.Cube.java class
 *
 * @author Qi Qi <qiq@kth.se>
 */
public class TestCube {

    /**
     * Solved pieces for the blue cube.
     */
    private final int[][] sp0 = {
        {0, 0, 1, 0, 0},
        {0, 1, 1, 1, 0},
        {1, 1, 1, 1, 1},
        {0, 1, 1, 1, 0},
        {0, 0, 1, 0, 0}
    };

    private final int[][] sp1 = {
        {0, 0, 1, 0, 1},
        {1, 1, 1, 1, 1},
        {0, 1, 1, 1, 0},
        {1, 1, 1, 1, 1},
        {0, 1, 0, 1, 1}
    };

    private final int[][] sp2 = {
        {0, 1, 0, 1, 0},
        {0, 1, 1, 1, 0},
        {1, 1, 1, 1, 1},
        {0, 1, 1, 1, 0},
        {0, 0, 1, 0, 0}
    };

    private final int[][] sp3 = {
        {1, 0, 1, 0, 1},
        {1, 1, 1, 1, 1},
        {0, 1, 1, 1, 0},
        {1, 1, 1, 1, 1},
        {1, 0, 1, 0, 1}
    };

    private final int[][] sp4 = {
        {0, 0, 1, 0, 0},
        {1, 1, 1, 1, 1},
        {0, 1, 1, 1, 0},
        {1, 1, 1, 1, 1},
        {0, 1, 0, 1, 1}
    };

    private final int[][] sp5 = {
        {0, 1, 0, 1, 0},
        {1, 1, 1, 1, 0},
        {0, 1, 1, 1, 1},
        {1, 1, 1, 1, 0},
        {0, 1, 0, 1, 1}
    };
    
    public final int[][][] allSolvedInputMatrix = {sp0, sp1, sp2, sp3, sp4, sp5};
    
    /**
     * Test checkSolved method. Here we construct a solved cube with correct
     * piece permutation and state sets. checkSolved() method should return
     * true. Then we construct a not solved cube, checkSolved() method should
     * return false.
     *
     * @throws IOException Output file writing errors.
     */
    @Test
    public void checkSolvedTest() throws IOException {
        //Check solved
        Piece[] pieces = new Piece[allSolvedInputMatrix.length];
        for (int i = 0; i < pieces.length; i++) {
            pieces[i] = new Piece(allSolvedInputMatrix[i]);
        }
        Cube cube = new Cube(pieces);
        int[][][] unfoldedFormFaceMatrix = cube.getUnfoldedFormFaceMatrix();

        assertTrue(cube.checkSolved());
        // Here we will get a "TestOutput.temp" file as long as the terminal output.
        System.out.println("Solved solution:\n" + Util.printCube("TestOutput.temp", unfoldedFormFaceMatrix));

        /**
         * Uncomment the following line if we want to delete the temp file
         * after testing
         */
        //Files.deleteIfExists(Paths.get("TestOutput.temp")); // delete temporary output file
        
        
        //Check unsolved
        int[][] zeros = new int[5][5];
        int[][][] unSolvedInputMatrix = {sp0, sp1, sp2, sp3, sp4, zeros};
        Piece[] pieces2 = new Piece[allSolvedInputMatrix.length];
        for (int i = 0; i < pieces2.length; i++) {
            pieces2[i] = new Piece(unSolvedInputMatrix[i]);
        }
        Cube cube2 = new Cube(pieces2);
        assertFalse(cube2.checkSolved());
    }
}
