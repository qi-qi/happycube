/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.happycube.Util;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * This class tests methods in com.happycube.Util.java class
 *
 * @author Qi Qi <qiq@kth.se>
 */
public class TestUtil {
    /**
     * Test rotateMatrixClockWise90 method.
     */
    @Test
    public void rotateMatrixClockWise90Test() {
        int[][] matrix = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        };

        int[][] testMatrix = {
            {7, 4, 1},
            {8, 5, 2},
            {9, 6, 3}
        };

        int[][] resultMatrix = Util.rotateMatrixClockWise90(matrix);

        assertEquals(testMatrix.length, resultMatrix.length);
        for (int i = 0; i < testMatrix.length; i++) {
            assertArrayEquals(testMatrix[i], resultMatrix[i]);
        }
    }
    /**
     * Test mirrorMatrix method.
     */
    @Test
    public void mirrorMatrixTest() {
        int[][] matrix = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        };

        int[][] testMatrix = {
            {3, 2, 1},
            {6, 5, 4},
            {9, 8, 7}
        };

        int[][] resultMatrix = Util.mirrorMatrix(matrix);

        assertEquals(testMatrix.length, resultMatrix.length);
        for (int i = 0; i < testMatrix.length; i++) {
            assertArrayEquals(testMatrix[i], resultMatrix[i]);
        }
    }

    /**
     * Test deepCopyMatrixTest method.
     */
    @Test
    public void deepCopyMatrixTest() {
        int[][] testMatrix = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        };

        int[][] resultMatrix = Util.deepCopyMatrix(testMatrix);

        assertEquals(testMatrix.length, resultMatrix.length);
        for (int i = 0; i < testMatrix.length; i++) {
            assertArrayEquals(testMatrix[i], resultMatrix[i]);
        }

        testMatrix[0][0] = -1;
        assertNotEquals(testMatrix[0][0], resultMatrix[0][0]);
    }
    
    /**
     * Test permuteString method.
     */
    @Test
    public void permuteStringTest() {
        Set<String> testSet = new TreeSet<String>() {
            {
                add("abc");
                add("acb");
                add("bac");
                add("bca");
                add("cab");
                add("cba");
            }
        };
        List<String> resultList = Util.permuteString("abc");
        Set<String> resultSet = new TreeSet<>(resultList);
        
        assertEquals(testSet, resultSet);
    }
    
    /**
     * Test digitMaskingTest method.
     */
    @Test
    public void digitMaskingTest() {
        int[] testArray = {0, 0, 0, 0, 1, 2};
        int[] resultArray = Util.digitMasking(10, 7, 6);
        assertArrayEquals(testArray, resultArray);
    }
    
    /**
     * Test stringToDigitArray method.
     */
    @Test
    public void stringToDigitArrayTest(){
        String input = "1234567890";
        int[] testArray = {1,2,3,4,5,6,7,8,9,0};
        int[] resultArray = Util.stringToDigitArray(input);
        assertArrayEquals(testArray, resultArray);
    }
}
