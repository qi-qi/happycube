/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.happycube;

/**
 * This is the main class to run the program.
 *
 * @author Qi Qi <qiq@kth.se>
 */
public class Main {

    public static void main(String[] args) {
        try {
            Solver solver;
            if (args.length == 1 && args[0].equalsIgnoreCase("true")) {
                // Shuffle the input and get different result each time.
                solver = new Solver("Output-Shuffle.txt", InputMatrix.getAllInputMatrixShuffled());
            } else {
                // No shuffling, get the same result each time by default
                solver = new Solver("Output.txt", InputMatrix.getAllInputMatrix());
            }

            if (solver.run() == true) {
                System.out.println("Happy Cube: Solved.");
            } else {
                System.out.println("Happy Cube: No Solution.");
            }
        } catch (Exception ex) {
            System.out.println("Happy Cube Exception: " + ex);
        } finally {
            System.exit(0);
        }
    }
}
