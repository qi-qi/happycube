/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.happycube;

/**
 * This is the cube model class.
 *
 * @author Qi Qi <qiq@kth.se>
 */
public class Cube {
    /**
     *   Cube:
     *           E----------F
     *          /|         /|
     *         / |        / |
     *        A----------B  |
     *        |  |       |  |
     *        |  G-------|--H
     *        | /        | /
     *        |/         |/
     *        C----------D
     *       
     */

    //The dimension of the piece. e.g. 5*5 Matrix => dimension: 5
    private final int dimension = 5;

    //6 permuted faces used to construct a cube.
    private final Piece[] faces;

    /**
     * Constructor for a cube. Use 6 permuted faces used to construct the cube.
     *
     * 6 faces in a cube. Note: Faces and related edges are directional, the
     * order of the letter indicates the order of the array elements. Therefore,
     * the positions of edges and vertices are calculated from the faces with
     * the following predefined positions:
     * 
     * Front:   Face 0 - ABDC
     * Right:   Face 1 - BFHD
     * Back:    Face 2 - FEGH
     * Left:    Face 3 - EACG
     * Top:     Face 4 - EFBA
     * Bottom:  Face 5 - GHDC
     * 
     * A---B    B---F   F---E   E---A   E---F   G---H
     * |f0 |    |f1 |   |f2 |   |f3 |   |f4 |   |f5 |
     * C---D    D---H   H---G   G---C   A---B   C---D
     *
     * @param faces 6 permuted faces used to construct the cube. 
     * The length of the piece array is six.
     */
    public Cube(Piece[] faces) {
        this.faces = faces;
    }

    /**
     * Check if the cube has been solved with current constructing faces and
     * faces' current status. "Solved cube" means all the elements on the edge
     * array are 1.
     *
     * @return True - if the cube has been solved with current constructing
     * faces' current status. False - cube not solved
     */
    public boolean checkSolved() {
        for (int[] edge : getEdgesAll()) {
            for (int element : edge) {
                if (element != 1) {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Return a array of "T" shape unfolded Form Face Matrix shown below. (Face
     * matrix has been mirrored and rotated)
     * 
     * UnfoldedFormFaceMatrix: M[0~5]
     *  A-------------B-------------F-------------E
     *  |             |             |             |
     *  |    [M0]     |    [M1]     |    [M2]     |
     *  |  f0-Current | f0-Current  | f0-Current  |
     *  |             |             |             |
     *  |             |             |             |
     *  C-------------D-------------H-------------G
     *                |             |
     *                |    [M3]     |
     *                |f5-mirrorRt90|
     *                |             |
     *                |             |
     *                C-------------G
     *                |             |
     *                |    [M4]     |
     *                |f3-rotate180 |
     *                |             |
     *                |             |
     *                A-------------E
     *                |             |
     *                |    [M5]     |
     *                | f4-rotate90 |
     *                |             |
     *                |             |
     *                B-------------F
     * 
     * @return a array of "T" shape unfolded Form Face Matrix.
     */
    
    public int[][][] getUnfoldedFormFaceMatrix(){
        int[][][] result = new int[6][dimension][dimension];
        //M0: face0-Current State
        result[0] = Util.deepCopyMatrix(faces[0].getCurrentStateMatrix());
        //M1: face1-Current State
        result[1] = Util.deepCopyMatrix(faces[1].getCurrentStateMatrix());
        //M2: face1-Current State
        result[2] = Util.deepCopyMatrix(faces[2].getCurrentStateMatrix());
        //M3: face5-Current State -> Mirror -> rotate 90 clockwise
        result[3] = Util.deepCopyMatrix(Util.rotateMatrixClockWise90(Util.mirrorMatrix(faces[5].getCurrentStateMatrix()))); 
        //M4: face3-Current State -> rotate 180 clockwise
        result[4] = Util.deepCopyMatrix(Util.rotateMatrixClockWise90(Util.rotateMatrixClockWise90(faces[3].getCurrentStateMatrix())));
        //M5: face4-Current State -> rotate 90 clockwise
        result[5] = Util.deepCopyMatrix(Util.rotateMatrixClockWise90(faces[4].getCurrentStateMatrix()));
        return result;
    }

    private int[][] getEdgesAll() {
        int[][] result = {
            getEdgeAB(), getEdgeAC(), getEdgeBD(),
            getEdgeBF(), getEdgeCD(), getEdgeDH(),
            getEdgeEA(), getEdgeEF(), getEdgeEG(),
            getEdgeFH(), getEdgeGC(), getEdgeGH()
        };
        return result;

    }

    /**
     * Vertex: Each value of vertices summed from three faces.
     */
    private int getVertexA() {
        return faces[0].getVertexUpLeft() + faces[3].getVertexUpRight() + faces[4].getVertexDownLeft();
    }

    private int getVertexB() {
        return faces[0].getVertexUpRight() + faces[1].getVertexUpLeft() + faces[4].getVertexDownRight();
    }

    private int getVertexC() {
        return faces[0].getVertexDownLeft() + faces[3].getVertexDownRight() + faces[5].getVertexDownLeft();
    }

    private int getVertexD() {
        return faces[0].getVertexDownRight() + faces[1].getVertexDownLeft() + faces[5].getVertexDownRight();
    }

    private int getVertexE() {
        return faces[2].getVertexUpRight() + faces[3].getVertexUpLeft() + faces[4].getVertexUpLeft();
    }

    private int getVertexF() {
        return faces[1].getVertexUpRight() + faces[2].getVertexUpLeft() + faces[4].getVertexUpRight();
    }

    private int getVertexG() {
        return faces[2].getVertexDownRight() + faces[3].getVertexDownLeft() + faces[5].getVertexUpLeft();
    }

    private int getVertexH() {
        return faces[1].getVertexDownRight() + faces[2].getVertexDownLeft() + faces[5].getVertexUpRight();
    }

    /**
     * Edge: result[0] and result[dimension-1] are vertices; their values summed
     * from three faces result[1] ~ result[dimension-2] are edges; their values
     * summed from two adjacent faces
     */
    private int[] getEdgeAB() {
        int[] result = new int[dimension];
        result[0] = getVertexA();
        result[dimension - 1] = getVertexB();
        for (int i = 1; i <= dimension - 2; i++) {
            result[i] = faces[0].getUpSide()[i] + faces[4].getDownSide()[i];
        }
        return result;
    }

    private int[] getEdgeAC() {
        int[] result = new int[dimension];
        result[0] = getVertexA();
        result[dimension - 1] = getVertexC();
        for (int i = 1; i <= dimension - 2; i++) {
            result[i] = faces[0].getLeftSide()[i] + faces[3].getRightSide()[i];
        }
        return result;
    }

    private int[] getEdgeEA() {
        int[] result = new int[dimension];
        result[0] = getVertexE();
        result[dimension - 1] = getVertexA();
        for (int i = 1; i <= dimension - 2; i++) {
            result[i] = faces[3].getUpSide()[i] + faces[4].getLeftSide()[i];
        }
        return result;
    }

    private int[] getEdgeBD() {
        int[] result = new int[dimension];
        result[0] = getVertexB();
        result[dimension - 1] = getVertexD();
        for (int i = 1; i <= dimension - 2; i++) {
            result[i] = faces[0].getRightSide()[i] + faces[1].getLeftSide()[i];
        }
        return result;
    }

    private int[] getEdgeBF() {
        int[] result = new int[dimension];
        result[0] = getVertexB();
        result[dimension - 1] = getVertexF();
        for (int i = 1; i <= dimension - 2; i++) {
            result[i] = faces[1].getUpSide()[i] + faces[4].getRightSide()[dimension - 1 - i];
        }
        return result;
    }

    private int[] getEdgeCD() {
        int[] result = new int[dimension];
        result[0] = getVertexC();
        result[dimension - 1] = getVertexD();
        for (int i = 1; i <= dimension - 2; i++) {
            result[i] = faces[0].getDownSide()[i] + faces[5].getDownSide()[i];
        }
        return result;
    }

    private int[] getEdgeGC() {
        int[] result = new int[dimension];
        result[0] = getVertexG();
        result[dimension - 1] = getVertexC();
        for (int i = 1; i <= dimension - 2; i++) {
            result[i] = faces[3].getDownSide()[i] + faces[5].getLeftSide()[i];
        }
        return result;
    }

    private int[] getEdgeDH() {
        int[] result = new int[dimension];
        result[0] = getVertexD();
        result[dimension - 1] = getVertexH();
        for (int i = 1; i <= dimension - 2; i++) {
            result[i] = faces[1].getDownSide()[i] + faces[5].getRightSide()[dimension - 1 - i];
        }
        return result;
    }

    private int[] getEdgeEF() {
        int[] result = new int[dimension];
        result[0] = getVertexE();
        result[dimension - 1] = getVertexF();
        for (int i = 1; i <= dimension - 2; i++) {
            result[i] = faces[4].getUpSide()[i] + faces[2].getUpSide()[dimension - 1 - i];
        }
        return result;
    }

    private int[] getEdgeEG() {
        int[] result = new int[dimension];
        result[0] = getVertexE();
        result[dimension - 1] = getVertexG();
        for (int i = 1; i <= dimension - 2; i++) {
            result[i] = faces[2].getRightSide()[i] + faces[3].getLeftSide()[i];
        }
        return result;
    }

    private int[] getEdgeFH() {
        int[] result = new int[dimension];
        result[0] = getVertexF();
        result[dimension - 1] = getVertexH();
        for (int i = 1; i <= dimension - 2; i++) {
            result[i] = faces[1].getRightSide()[i] + faces[2].getLeftSide()[i];
        }
        return result;
    }

    private int[] getEdgeGH() {
        int[] result = new int[dimension];
        result[0] = getVertexG();
        result[dimension - 1] = getVertexH();
        for (int i = 1; i <= dimension - 2; i++) {
            result[i] = faces[5].getUpSide()[i] + faces[2].getDownSide()[dimension - 1 - i];
        }
        return result;
    }

}
