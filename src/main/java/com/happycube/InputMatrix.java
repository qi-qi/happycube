/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.happycube;

import java.util.List;
import java.util.Random;

/**
 *
 * @author Qi Qi <qiq@kth.se>
 */
public class InputMatrix {

    /**
     * Input pieces for the blue cube.
     */
    private static final int[][] p0 = {
        {0, 0, 1, 0, 0},
        {0, 1, 1, 1, 0},
        {1, 1, 1, 1, 1},
        {0, 1, 1, 1, 0},
        {0, 0, 1, 0, 0}
    };

    private static final int[][] p1 = {
        {1, 0, 1, 0, 1},
        {1, 1, 1, 1, 1},
        {0, 1, 1, 1, 0},
        {1, 1, 1, 1, 1},
        {1, 0, 1, 0, 1}
    };

    private static final int[][] p2 = {
        {0, 0, 1, 0, 0},
        {0, 1, 1, 1, 1},
        {1, 1, 1, 1, 0},
        {0, 1, 1, 1, 1},
        {0, 0, 1, 0, 0}
    };

    private static final int[][] p3 = {
        {0, 1, 0, 1, 0},
        {1, 1, 1, 1, 0},
        {0, 1, 1, 1, 1},
        {1, 1, 1, 1, 0},
        {1, 1, 0, 1, 0}
    };

    private static final int[][] p4 = {
        {0, 1, 0, 1, 0},
        {1, 1, 1, 1, 1},
        {0, 1, 1, 1, 0},
        {1, 1, 1, 1, 1},
        {1, 0, 1, 0, 0}
    };

    private static final int[][] p5 = {
        {0, 1, 0, 1, 0},
        {0, 1, 1, 1, 1},
        {1, 1, 1, 1, 0},
        {0, 1, 1, 1, 1},
        {1, 1, 0, 1, 1}
    };

    /**
     * Get an array of all input matrix.
     *
     * @return An array of all input matrix.
     */
    public static int[][][] getAllInputMatrix() {
        int[][][] result = {p0, p1, p2, p3, p4, p5};
        return result;
    }

    /**
     * Get a shuffled array of all input matrix. (so that we can get different
     * solutions each time when solved)
     *
     * @return A shuffled array of all input matrix.
     */
    public static int[][][] getAllInputMatrixShuffled() {
        int[][][] allMatrix = {p0, p1, p2, p3, p4, p5};
        int[][][] result = new int[6][5][5];
        List<String> permutation = Util.permuteString("012345");
        Random random = new Random();
        int randomIdx = random.nextInt(permutation.size());
        int[] permutationIdx = Util.stringToDigitArray(permutation.get(randomIdx));
        for (int i = 0; i < 6; i++) {
            int idx = permutationIdx[i];
            result[i] = allMatrix[idx];
        }
        return result;
    }
}
