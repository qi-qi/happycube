/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.happycube;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * This class contains utility methods.
 *
 * @author Qi Qi <qiq@kth.se>
 */
public class Util {

    /**
     * Rotate the N*N matrix by 90 degree clockwise.
     * Transform: (i, j) => (j, N - 1 - i), where N is the number of rows
     *    Matrix             Rotated
     *  1   2   3   |  |    7   4   1
     *  4   5   6   |=>|    8   5   2
     *  7   8   9   |  |    9   6   3
     *
     * @param matrix The input matrix.
     * @return A new rotated matrix deep copy by 90 degree clockwise.
     */
    public static int[][] rotateMatrixClockWise90(int[][] matrix) {
        int m = matrix.length;
        int[][] result = new int[m][m];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                result[j][m - 1 - i] = matrix[i][j];
            }
        }
        return result;
    }

    /**
     * Mirror the N*N matrix.
     * Transform: (i, j) => (i, N - 1 - j) where N is the number of columns
     *    Matrix             Mirror
     *  1   2   3   |  |    3   2   1
     *  4   5   6   |=>|    6   5   4
     *  7   8   9   |  |    9   8   7
     *
     * @param matrix The input matrix.
     * @return A new mirrored deep copy matrix.
     */
    public static int[][] mirrorMatrix(int[][] matrix) {
        int m = matrix.length;
        int[][] result = new int[m][m];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                result[i][m - 1 - j] = matrix[i][j];
            }
        }
        return result;
    }

    /**
     * Deep copy of a matrix.
     *
     * @param matrix The input matrix.
     * @return A new deep copy matrix.
     */
    public static int[][] deepCopyMatrix(int[][] matrix) {
        int row = matrix.length;
        int col = matrix[0].length;
        int[][] result = new int[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                result[i][j] = matrix[i][j];
            }
        }
        return result;
    }

    /**
     * Print the cube "unfolded form" solution to an external file, and also
     * return the printed result string.
     *
     * @param resultDstPath The output file path.
     * @param srcUnfoldedFormFaceMatrix The source array of the unfolded Form
     * Face Matrix.
     * @return The result string of printed to the output file.
     * @throws IOException if the named file "resultDstPath" exists but is a
     * directory rather than a regular file, does not exist but cannot be
     * created, or cannot be opened for any other reason
     */
    public static String printCube(String resultDstPath, int[][][] srcUnfoldedFormFaceMatrix) throws IOException {
        StringBuilder resultString = new StringBuilder();
        try (PrintWriter resultWriter = new PrintWriter(new FileWriter(resultDstPath), true)) {
            int dimension = srcUnfoldedFormFaceMatrix[0].length;
            int[][] resultMatrix = new int[4 * dimension][3 * dimension];
            int[][] m0 = srcUnfoldedFormFaceMatrix[0];
            int[][] m1 = srcUnfoldedFormFaceMatrix[1];
            int[][] m2 = srcUnfoldedFormFaceMatrix[2];
            int[][] m3 = srcUnfoldedFormFaceMatrix[3];
            int[][] m4 = srcUnfoldedFormFaceMatrix[4];
            int[][] m5 = srcUnfoldedFormFaceMatrix[5];
            int[][] zeros = new int[dimension][dimension];

            for (int i = 0; i < dimension; i++) {
                System.arraycopy(m0[i], 0, resultMatrix[i], 0, dimension);
                System.arraycopy(m1[i], 0, resultMatrix[i], dimension, dimension);
                System.arraycopy(m2[i], 0, resultMatrix[i], 2 * dimension, dimension);
            }

            for (int i = 0; i < dimension; i++) {
                System.arraycopy(zeros[i], 0, resultMatrix[i + dimension], 0, dimension);
                System.arraycopy(m3[i], 0, resultMatrix[i + dimension], dimension, dimension);
                System.arraycopy(zeros[i], 0, resultMatrix[i + dimension], 2 * dimension, dimension);
            }

            for (int i = 0; i < dimension; i++) {
                System.arraycopy(zeros[i], 0, resultMatrix[i + 2 * dimension], 0, dimension);
                System.arraycopy(m4[i], 0, resultMatrix[i + 2 * dimension], dimension, dimension);
                System.arraycopy(zeros[i], 0, resultMatrix[i + 2 * dimension], 2 * dimension, dimension);
            }

            for (int i = 0; i < dimension; i++) {
                System.arraycopy(zeros[i], 0, resultMatrix[i + 3 * dimension], 0, dimension);
                System.arraycopy(m5[i], 0, resultMatrix[i + 3 * dimension], dimension, dimension);
                System.arraycopy(zeros[i], 0, resultMatrix[i + 3 * dimension], 2 * dimension, dimension);
            }

            for (int row = 0; row < resultMatrix.length; row++) {
                for (int col = 0; col < resultMatrix[row].length; col++) {
                    if (resultMatrix[row][col] == 1) {
                        resultWriter.print("o");
                        resultString.append("o");
                    } else {
                        resultWriter.print(" ");
                        resultString.append(" ");
                    }
                }
                resultWriter.println();
                resultString.append("\n");
            }

            resultWriter.close();
        }
        return resultString.toString();
    }

    /**
     * Permute a string and return the result list of permutations. e.g. input:
     * "abc", result: {"abc", "acb", "bac", "bca", "cab", "cba"}
     *
     * @param input The string to be permuted
     * @return The result list of permutations.
     */
    public static List<String> permuteString(String input) {
        List<String> result = new LinkedList<>();
        int length = input.length();
        boolean[] usedFlags = new boolean[length];
        StringBuilder output = new StringBuilder();
        permuteString(input, result, output, length, usedFlags);

        return result;
    }

    // private recursive method used by permuteString
    private static void permuteString(String input, List<String> result, StringBuilder output, int length, boolean[] usedFlags) {
        if (output.length() == length) {
            result.add(output.toString());
            return;
        }
        for (int i = 0; i < length; i++) {
            if (usedFlags[i] == true) {
                continue;
            }
            output.append(input.charAt(i));
            usedFlags[i] = true;
            permuteString(input, result, output, length, usedFlags);
            usedFlags[i] = false;
            output.setLength(output.length() - 1);
        }
    }

    /**
     * Transform a number into a single digit masking form (base 1-9). e.g.
     * Input: 10, Digit: 7 (count from 0 to 7, 8 different values per digit),
     * Length: 6 -> Result: {0,0,0,0,1,2}
     *
     * @param number Input number to be masked
     * @param digit Digit used to mask the input number, range from 1-9
     * @param length Length of the masking (zero padding)
     * @return Digit masked array of the input number
     */
    public static int[] digitMasking(int number, int digit, int length) {
        int[] result = new int[length];
        for (int i = 0; i < length; i++) {
            result[length - 1 - i] = number & digit;
            number /= (digit + 1);
        }
        return result;
    }

    /**
     * Transform an input string into a digit integer array. e.g. Input: "012345"
     * -> Result: {0,1,2,3,4,5}
     *
     * @param input Input string needed to be transformed into a digit integer
     * array.
     * @return The transformed digit integer array.
     */
    public static int[] stringToDigitArray(String input) {
        String number = input.replaceAll("[^0-9]", ""); // clean the string
        int[] result = new int[number.length()];
        for (int i = 0; i < result.length; i++) {
            result[i] = Character.getNumericValue(number.charAt(i));
        }
        return result;
    }
}
