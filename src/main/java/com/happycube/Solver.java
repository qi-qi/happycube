/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.happycube;

import java.io.IOException;
import java.util.List;

/**
 * This is the class to get the happy cube solution.
 *
 * @author Qi Qi <qiq@kth.se>
 */
public class Solver {

    private final int[][][] allInputMatrix; // All initial input cube pieces
    private final String resultDstPath; // output file path

    public Solver(String resultDstPath, int[][][] allInputMatrix) {
        this.allInputMatrix = allInputMatrix;
        this.resultDstPath = resultDstPath;
    }

    public boolean run() throws IOException {
        /**
         * 1) We get all permutation of the pieces. For a cube, it has 6 faces.
         * So for 6 input pieces, there are 6! = 720 permutations. We use
         * integers 0 ~ 5 to represent the permutation code.
         */
        List<String> allPiecePermutations = Util.permuteString("012345");
        for (String permutationString : allPiecePermutations) {
            int[] permutaion = Util.stringToDigitArray(permutationString);

            // set pieces permutation
            Piece[] pieces = new Piece[allInputMatrix.length];
            for (int i = 0; i < pieces.length; i++) {
                int permutaionIdx = permutaion[i];
                pieces[i] = new Piece(allInputMatrix[permutaionIdx]);
            }

            /**
             * 2) We try all different possible state sets within each
             * permutation. One piece has 8 states(see Piece.java class), one
             * permutation has 6 pieces, therefore, we have 8^6=262144 possible
             * states for each permutation.
             */
            for (int stateCode = 0; stateCode < 262144; stateCode++) {
                int[] currentStates = Util.digitMasking(stateCode, 7, 6);
                for (int i = 0; i < pieces.length; i++) {
                    int pieceState = currentStates[i];
                    pieces[i].setCurrentState(pieceState);
                }
                /**
                 * 3) We make a new cube with current piece permutation and
                 * current state sets. Then check if the cube has been solved.
                 * "Solved cube" means all the elements on the edge array are 1.
                 * If solved, return true and print out the cube; otherwise,
                 * move on until all the permutations with all possible state
                 * set have been tried out.
                 */
                Cube cube = new Cube(pieces);
                if (cube.checkSolved() == true) {
                    // found solution and print out result
                    Util.printCube(resultDstPath, cube.getUnfoldedFormFaceMatrix());
                    return true; // True: found solution
                }
            }
        }
        return false; // False: Tried out all possible piece permutation and state sets, still no solutions
    }

}
