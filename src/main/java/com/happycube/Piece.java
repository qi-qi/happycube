/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.happycube;

/**
 *
 * @author Qi Qi <qiq@kth.se>
 */
public class Piece {
    /*
     *   Piece:
     *             UpSide
     *           ---------
     *           |       |
     *  LeftSide |       | RightSide
     *           |       |
     *           ---------
     *            DownSide
     *       
     */

    /**
     * One piece will have 8 states in total. 4 different positions from
     * rotations, and mirrors of these 4 positions.
     * State codes:
     * State 0: origin matrix
     * State 1: origin matrix rotated 90 degree clockwise
     * State 2: origin matrix rotated 180 degree clockwise
     * State 3: origin matrix rotated 270 degree clockwise
     * State 4: mirrored matrix 
     * State 5: mirrored matrix rotated 90 degree clockwise
     * State 6: mirrored matrix rotated 180 degree clockwise
     * State 7: mirrored matrix rotated 270 degree clockwise
     * Set the state of current matrix.
     */
    private final int[][][] matrixAllStates;
    private int currentState;

    /*
     * The dimension of the piece. e.g. 5*5 Matrix => dimension: 5
     */
    private final int dimension = 5;

    /**
     * Constructor for a new piece.
     *
     * @param matrixInitialState The initial piece matrix.
     */
    public Piece(int[][] matrixInitialState) {
        currentState = 0;
        matrixAllStates = new int[8][dimension][dimension];
        
        // Deep copy matrix
        // State 0: origin matrix
        matrixAllStates[0] = Util.deepCopyMatrix(matrixInitialState);
        // State 1: origin matrix rotated 90 degree clockwise
        matrixAllStates[1] = Util.rotateMatrixClockWise90(matrixAllStates[0]);
        // State 2: origin matrix rotated 180 degree clockwise
        matrixAllStates[2] = Util.rotateMatrixClockWise90(matrixAllStates[1]);
        // State 3: origin matrix rotated 270 degree clockwise
        matrixAllStates[3] = Util.rotateMatrixClockWise90(matrixAllStates[2]);
        // State 4: mirrored matrix 
        matrixAllStates[4] = Util.mirrorMatrix(matrixInitialState);
        // State 5: mirrored matrix rotated 90 degree clockwise
        matrixAllStates[5] = Util.rotateMatrixClockWise90(matrixAllStates[4]);
        // State 6: mirrored matrix rotated 180 degree clockwise
        matrixAllStates[6] = Util.rotateMatrixClockWise90(matrixAllStates[5]);
        // State 7: mirrored matrix rotated 270 degree clockwise
        matrixAllStates[7] = Util.rotateMatrixClockWise90(matrixAllStates[6]);
    }

    /**
     * Set the state of current matrix. One piece will have 8 states in total. 4
     * different positions from rotations, and mirrors of these 4 positions.
     * State codes:
     * State 0: origin matrix
     * State 1: origin matrix rotated 90 degree clockwise
     * State 2: origin matrix rotated 180 degree clockwise
     * State 3: origin matrix rotated 270 degree clockwise
     * State 4: mirrored matrix 
     * State 5: mirrored matrix rotated 90 degree clockwise
     * State 6: mirrored matrix rotated 180 degree clockwise
     * State 7: mirrored matrix rotated 270 degree clockwise
     * Set the state of current matrix.
     * @param currentState The current state of the piece.
     */
    public void setCurrentState(int currentState) {
        this.currentState = currentState;
    }
    
    public int[] getUpSide() {
        int[] result = new int[dimension];
        for (int i = 0; i < result.length; i++) {
            result[i] = matrixAllStates[currentState][0][i];
        }
        return result;
    }

    public int[] getLeftSide() {
        int[] result = new int[dimension];
        for (int i = 0; i < result.length; i++) {
            result[i] = matrixAllStates[currentState][i][0];
        }
        return result;
    }

    public int[] getRightSide() {
        int[] result = new int[dimension];
        for (int i = 0; i < result.length; i++) {
            result[i] = matrixAllStates[currentState][i][dimension - 1];
        }
        return result;
    }

    public int[] getDownSide() {
        int[] result = new int[dimension];
        for (int i = 0; i < result.length; i++) {
            result[i] = matrixAllStates[currentState][dimension - 1][i];
        }
        return result;
    }

    public int getVertexUpLeft() {
        return getUpSide()[0];
    }

    public int getVertexUpRight() {
        return getUpSide()[dimension - 1];
    }

    public int getVertexDownLeft() {
        return getDownSide()[0];
    }

    public int getVertexDownRight() {
        return getDownSide()[dimension - 1];
    }

    public int[][][] getMatrixAllStates() {
        return matrixAllStates;
    }

    public int[][] getCurrentStateMatrix() {
        return matrixAllStates[currentState];
    }

}
