HappyCube
==========================================================

|Test Task              | Author            | Requirements
|-----------------------|-------------------|--------------
|Java Backend Engineer  |Qi Qi <qiq@kth.se> | JRE 7 or above

Two Ways to Run the Program:
==========================================================
1) By default, run:

(The result "Output.txt" will appear in the same folder)

```
java -jar HappyCube-1.0-SNAPSHOT.jar
```

2) If we want to get different results each time with shuffled input pieces, run: 

(The result "Output-Shuffle.txt" will appear in the same folder)

```
java -jar HappyCube-1.0-SNAPSHOT.jar true
```